

if (typeof CCF === 'undefined') {
	CCF = {};
}

if (typeof CCF.wbSceneDownload === 'undefined') {
	CCF.wbSceneDownload = {

		ENABLED_URL: window.location.protocol + "//" + window.location.host + "/xapi/workbenchSceneDownloadConfig/" + XNAT.data.context.projectID + "/isEnabled",
		API_URL: window.location.protocol + "//" + window.location.host + "/xapi/workbenchSceneDownload/project/" + XNAT.data.context.projectID + "/experiment/" + XNAT.data.context.ID,
		LIST_SCENE_FILES_URL: window.location.protocol + "//" + window.location.host + "/xapi/workbenchSceneDownload/project/" + XNAT.data.context.projectID + "/experiment/" + XNAT.data.context.ID + "/listSceneFiles",
		LIST_SCENES_URL: window.location.protocol + "//" + window.location.host + "/xapi/workbenchSceneDownload/project/" + XNAT.data.context.projectID + "/experiment/" + XNAT.data.context.ID + "/listScenes?sceneFile=",
		DOWNLOAD_SCENE_FILE_URL: window.location.protocol + "//" + window.location.host + "/xapi/workbenchSceneDownload/project/" + XNAT.data.context.projectID + "/experiment/" + XNAT.data.context.ID + "/downloadSceneFile?sceneFile=",
		DOWNLOAD_SCENE_URL: window.location.protocol + "//" + window.location.host + "/xapi/workbenchSceneDownload/project/" + XNAT.data.context.projectID + "/experiment/" + XNAT.data.context.ID + "/downloadScene?sceneFile=",
		CURRENT_SCENE_FILES: [],
		//LIST_SCENE_FILES_URL: function() { return CCF.wbSceneDownload.API_URL + "/listSceneFiles" },
		DOWNLOAD_MODAL: {
		        id: 'downloadModal',
		        kind: 'fixed',  
		        width: 900, 
		        height: 425, 
		        scroll: 'yes', 
		        overflow: 'auto', 
		        title: 'Workbench Scene Download', 
			content: "<div id='wbscene-content' class='wbscene-loading'><div id='modal-msg' style='margin-right:15px'></div></div>",
		        ok: 'show',  
		        okLabel: "Done",
		        okAction: function(){ xmodal.close() },
		        cancel: 'hide' 
		},
		SELECTION_MODAL: {
		        id: 'selectionModal',
		        kind: 'fixed',  
		        width: 400, 
		        height: 225, 
		        scroll: 'yes', 
		        title: 'Select Action', 
			content: "<div id='wbsceneselect-content' class='wbsceneselect-loading'><div id='select-modal-msg' style='margin-right:15px'></div></div>",
		        ok: 'hide',  
		        okLabel: "Load Scenes",
		        okAction: function(){ xmodal.close() },
		        cancel: 'show' 
		}

	}
}


///////////////////////
///////////////////////
///////////////////////
// SHOW LINK METHODS //
///////////////////////
///////////////////////
///////////////////////


CCF.wbSceneDownload.checkShowLink = function() {

    XNAT.xhr.get({
	url:CCF.wbSceneDownload.ENABLED_URL,
	cache: false,
	async: true,
	context: this
    }).done( function(data, textStatus, jqXHR) {
	$(".wbSceneDownloadLink").show();
    }).fail( function(data, textStatus, jqXHR) {
	// Do nothing
    });

}


CCF.wbSceneDownload.workbenchSceneDownload = function() {

    var modalObj = CCF.wbSceneDownload.DOWNLOAD_MODAL;
    var msg="Searching for scene files.  This may take a minute...";
    if (typeof title != "undefined") {
        modalObj = JSON.parse(JSON.stringify(CCF.wbSceneDownload.DOWNLOAD_MODAL));
        modalObj.title = title;
    } 
    xModalOpenNew(modalObj);
    $("#wbscene-content").removeClass();
    $("#wbscene-content").addClass("wbscene-loading");
    $("#modal-msg").html(msg);
	//console.log(CCF.wbSceneDownload.LIST_SCENE_FILES_URL);

    XNAT.xhr.get({
	url:CCF.wbSceneDownload.LIST_SCENE_FILES_URL,
	cache: false,
	async: true,
	context: this
    }).done( function(data, textStatus, jqXHR) {
	//console.log(CCF.wbSceneDownload.LIST_SCENE_FILES_URL);
	//console.log(data);
	//console.log("jqXHR=",jqXHR);
	//console.log(CCF.wbSceneDownload.LIST_SCENE_FILES_URL);
    	$("#wbscene-content").html("<ul class='nav'></ul>");
    	$("#wbscene-content").removeClass();
    	$("#wbscene-content").addClass("wbscene-contents");
    	///$("#wbscene-content").closest(".body.content").style("overflow:auto");
	CCF.wbSceneDownload.CURRENT_SCENE_FILES = [];
	$.each(data, function(inx,value) {
		var resource = value.replace(/\/.*$/,"");
		var fname = value.replace(/^.*\//,"");
    		$("#wbscene-content").find('ul').first().append("<li style='padding-top:5px;padding-bottom:5px'><a href='#' data-inx=\"" + inx + 
			"\" onclick=\"CCF.wbSceneDownload.showSelectFile(this)\" style='width:100%'>" + fname + " (Resource=" + resource + ")</a></li>");
		var info = { inx: inx, scenefilepath: value, scenefilename: fname, sceneresourceresource: resource } 
		CCF.wbSceneDownload.CURRENT_SCENE_FILES.push(info);
		//$(value);
	});
    	//$("#wbscene-content").html(data);
    }).fail( function(data, textStatus, jqXHR) {
    	$("#modal-msg").html("ERROR:  Could not retreive list from server<br><br>" + jqXHR);
    });

}

CCF.wbSceneDownload.showSelectFile = function(sceneObj) {
	var modalObj = CCF.wbSceneDownload.SELECTION_MODAL;
	var sceneobj_inx = $(sceneObj).data("inx")
	if (typeof title != "undefined") {
		modalObj = JSON.parse(JSON.stringify(CCF.wbSceneDownload.SELECTION_MODAL));
		modalObj.title = title;
	} 
	xModalOpenNew(modalObj);
	CCF.wbSceneDownload.sceneObj = sceneObj;
	$("#wbsceneselect-content").removeClass();
	$("#wbsceneselect-content").addClass("wbsceneselect-contents");
	var msg="Make a selection.";
	$("#select-modal-msg").html(msg);
	$("#selectionModal").find(".buttons").prepend("<button tabindex=\"1\" id=\"selectionModal-download-button\" onclick=\"CCF.wbSceneDownload.downloadSceneFile(CCF.wbSceneDownload.sceneObj)\" class=\"ok processbtn default button\">Download</button>");
   	if ($(sceneObj).parent().find("ul[data-inx='" + sceneobj_inx + "']").length<1) {
		$("#selectionModal").find(".buttons").prepend("<button tabindex=\"2\" id=\"selectionModal-download-button\" onclick=\"CCF.wbSceneDownload.sceneFileSelect(CCF.wbSceneDownload.sceneObj)\" class=\"ok processbtn default button\">Load Scenes</button>");
	}

}

CCF.wbSceneDownload.showSelectScene = function(sceneObj) {
	$(".processbtn").prop("disabled", false);
	var modalObj = CCF.wbSceneDownload.SELECTION_MODAL;
	var sceneobj_inx = $(sceneObj).data("inx")
	if (typeof title != "undefined") {
		modalObj = JSON.parse(JSON.stringify(CCF.wbSceneDownload.SELECTION_MODAL));
		modalObj.title = title;
	} 
	xModalOpenNew(modalObj);
	CCF.wbSceneDownload.sceneObj = sceneObj;
	$("#wbsceneselect-content").removeClass();
	$("#wbsceneselect-content").addClass("wbsceneselect-contents");
	var msg="Make a selection.";
	$("#select-modal-msg").html(msg);
	$("#selectionModal").find(".buttons").prepend("<button tabindex=\"1\" id=\"selectionModal-download-button\" onclick=\"CCF.wbSceneDownload.downloadScene(CCF.wbSceneDownload.sceneObj)\" class=\"ok processbtn default button\">Download</button>");

}

CCF.wbSceneDownload.downloadSceneFile = function(sceneObj) {
	$(".processbtn").prop("disabled", true);
	var modalObj = CCF.wbSceneDownload.SELECTION_MODAL;
	var msg="Preparing download.  This may take a minute...";
	if (typeof title != "undefined") {
		modalObj = JSON.parse(JSON.stringify(CCF.wbSceneDownload.SELECTION_MODAL));
		modalObj.title = title;
	} 
	xModalOpenNew(modalObj);
	var sceneobj_inx = $(sceneObj).data("inx")
	var info = "";
	$.each(CCF.wbSceneDownload.CURRENT_SCENE_FILES, function(inx,value) {
		if (inx === sceneobj_inx) {
			info = value;
		}
	});
	$("#wbsceneselect-content").removeClass();
	$("#wbsceneselect-content").addClass("wbsceneselect-loading");
	$("#select-modal-msg").html(msg);
	var oReq = new XMLHttpRequest();
	//console.log(CCF.wbSceneDownload.DOWNLOAD_SCENE_FILE_URL + encodeURIComponent(info.scenefilepath));
	oReq.open("GET", CCF.wbSceneDownload.DOWNLOAD_SCENE_FILE_URL + encodeURIComponent(info.scenefilepath), true);
	oReq.responseType = "blob";
	oReq.onload = function(oEvent) {
		var blob = oReq.response;
		var requestStatus = oReq.status;
		if (requestStatus !== 200) {
    			$("#wbsceneselect-content").removeClass();
    			$("#wbsceneselect-content").addClass("wbscene-error");
    			$("#select-modal-msg").html("There was an error processing your request (HTTP-STATUS=" + requestStatus + ")");
			$(".processbtn").prop("disabled", false);
			return;
		}
		$("#select-modal-msg").html("Downloading file");
		//console.log(requestStatus);
		xmodal.close();
		var disposition = oReq.getResponseHeader('Content-Disposition');
		//console.log(disposition);
		var filename = disposition.replace(/^.*filename="*/,"").replace(/[.]/g,"_").replace(/"*;/g,"").replace(/[_]([^_]*)$/,'.' + '$1');
		//console.log(filename);
		var a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";
		url = window.URL.createObjectURL(blob);
		a.href = url;
		a.download = filename;
		a.click();
		window.URL.revokeObjectURL(url);
		$(".processbtn").prop("disabled", false);
	}
	oReq.send();
}

CCF.wbSceneDownload.downloadScene = function(sceneObj) {
	$(".processbtn").prop("disabled", true);
	var modalObj = CCF.wbSceneDownload.SELECTION_MODAL;
	var msg="Preparing download.  This may take a minute...";
	if (typeof title != "undefined") {
		modalObj = JSON.parse(JSON.stringify(CCF.wbSceneDownload.SELECTION_MODAL));
		modalObj.title = title;
	} 
	xModalOpenNew(modalObj);
	//console.log(sceneObj);
	$("#wbsceneselect-content").removeClass();
	$("#wbsceneselect-content").addClass("wbsceneselect-loading");
	$("#select-modal-msg").html(msg);
	var sceneobj_inx = $(sceneObj).data("inx")
	var sceneobj_scenenum = $(sceneObj).data("scenenum")
	var info = "";
	$.each(CCF.wbSceneDownload.CURRENT_SCENE_FILES, function(inx,value) {
		if (inx === sceneobj_inx) {
			info = value;
		}
	});
	var oReq = new XMLHttpRequest();
	//console.log(CCF.wbSceneDownload.DOWNLOAD_SCENE_URL + encodeURIComponent(info.scenefilepath));
	oReq.open("GET", CCF.wbSceneDownload.DOWNLOAD_SCENE_URL + encodeURIComponent(info.scenefilepath) + "&sceneNumber=" + sceneobj_scenenum, true);
	oReq.responseType = "blob";
	oReq.onload = function(oEvent) {
		var blob = oReq.response;
		var requestStatus = oReq.status;
		if (requestStatus !== 200) {
    			$("#wbsceneselect-content").removeClass();
    			$("#wbsceneselect-content").addClass("wbscene-error");
    			$("#select-modal-msg").html("There was an error processing your request (HTTP-STATUS=" + requestStatus + ")");
			$(".processbtn").prop("disabled", false);
			return;
		}
		$("#select-modal-msg").html("Downloading file");
		//console.log(requestStatus);
		xmodal.close();
		var disposition = oReq.getResponseHeader('Content-Disposition');
		//console.log(disposition);
		var filename = disposition.replace(/^.*filename="*/,"").replace(/[.]/g,"_").replace(/"*;/g,"").replace(/[_]([^_]*)$/,'.' + '$1');
		//console.log(filename);
		var a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";
		url = window.URL.createObjectURL(blob);
		a.href = url;
		a.download = filename;
		a.click();
		window.URL.revokeObjectURL(url);
		$(".processbtn").prop("disabled", false);
	}
	oReq.send();

}

CCF.wbSceneDownload.sceneFileSelect = function(sceneObj) {
	$(".processbtn").prop("disabled", true);
	var modalObj = CCF.wbSceneDownload.SELECTION_MODAL;
	var msg="Searching for scenes.  Please wait...";
	if (typeof title != "undefined") {
		modalObj = JSON.parse(JSON.stringify(CCF.wbSceneDownload.SELECTION_MODAL));
		modalObj.title = title;
	} 
	xModalOpenNew(modalObj);
	var sceneobj_inx = $(sceneObj).data("inx")
	var info = "";
	$.each(CCF.wbSceneDownload.CURRENT_SCENE_FILES, function(inx,value) {
		if (inx === sceneobj_inx) {
			info = value;
		}
	});
	$("#wbsceneselect-content").removeClass();
	$("#wbsceneselect-content").addClass("wbsceneselect-loading");
	$("#select-modal-msg").html(msg);

    XNAT.xhr.get({
	url:CCF.wbSceneDownload.LIST_SCENES_URL + encodeURIComponent(info.scenefilepath),
	cache: false,
	async: true,
	context: this
    }).done( function(data, textStatus, jqXHR) {
   	$(sceneObj).parent().append("<ul data-inx='" + sceneobj_inx + "'></ul>");
	$.each(data, function(inx,value) {
		var resource = value.replace(/\/.*$/,"");
		var fname = value.replace(/^.*\//,"");
		var sceneNum = inx+1; 
    		$(sceneObj).parent().find("ul[data-inx='" + sceneobj_inx + "']").append("<li style='padding-top:5px;padding-bottom:5px'><a href='#' onclick=\"CCF.wbSceneDownload.showSelectScene(this)\" " +
			"data-scenenum=\"" + sceneNum + "\" data-inx=\"" + sceneobj_inx + 
			"\" style='width:100%'>" + fname + " (Resource=" + resource + ")</a></li>");
	});
	xmodal.close();
	$(".processbtn").prop("disabled", false);
    }).fail( function(data, textStatus, jqXHR) {
    	$("#select-modal-msg").html("ERROR:  Could not retreive list from server<br><br>" + jqXHR);
	$(".processbtn").prop("disabled", false);
    });

}



