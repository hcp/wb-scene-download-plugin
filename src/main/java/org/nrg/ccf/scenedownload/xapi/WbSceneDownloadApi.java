package org.nrg.ccf.scenedownload.xapi;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.commons.utilities.exception.ScriptExecException;
import org.nrg.ccf.scenedownload.client.WbSceneHelperClient;
import org.nrg.ccf.scenedownload.components.ProjectPreferences;
import org.nrg.ccf.scenedownload.exception.WbSceneDownloadClientException;
import org.nrg.ccf.scenedownload.pojos.ClientCommand;
import org.nrg.ccf.scenedownload.pojos.ClientInfo;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@XapiRestController
@Api(description = "Workbench Scene Download Configuration API")
public class WbSceneDownloadApi extends AbstractXapiRestController {

	private final ProjectPreferences _preferences;
	private String TMPDIR_SUFFIX = "_SceneHelper";
    private static final String ATTACHMENT_DISPOSITION = "attachment; filename=\"%s\"";
	private static final String DOWNLOAD_MEDIA_TYPE = "application/x-download";
	
	@Autowired
	protected WbSceneDownloadApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
								final ProjectPreferences preferences) {
		super(userManagementService, roleHolder);
		_preferences = preferences;
	}

	@ApiOperation(value = "Gets WB Scene File List", notes = "Returns WB scene file list.", 
						responseContainer = "List", response = String.class)
    @ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(
    		value = {"/workbenchSceneDownload/project/{projectId}/experiment/{experimentId}/listSceneFiles"}, 
    		restrictTo=AccessLevel.Read,
    		produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> listSceneFiles(
    		@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("experimentId") final String experimentId
    		) throws NrgServiceException {
		if (!_preferences.getWbSceneDownloadEnabled(projectId)) {
			return new ResponseEntity<List<String>>(
					Arrays.asList(new String[] { "ERROR:  Scene file download is not enabled for this project" }),
					HttpStatus.BAD_REQUEST);
		}
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		final ClientInfo clientInfo = new ClientInfo(ClientCommand.LIST_SCENE_FILES, projectId, 
				mrSession.getLabel(), null, null);
    	try {
			final List<String> returnList = WbSceneHelperClient.executeCommand(clientInfo);
			return new ResponseEntity<List<String>>(returnList,HttpStatus.OK);
		} catch (WbSceneDownloadClientException | ScriptExecException e) {
			log.error(ExceptionUtils.getFullStackTrace(e));
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

	@ApiOperation(value = "Gets WB Scene File Scene List", notes = "Returns WB scene file scene list.", 
						responseContainer = "List", response = String.class)
    @ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(
    		value = {"/workbenchSceneDownload/project/{projectId}/experiment/{experimentId}/listScenes"}, 
    		restrictTo=AccessLevel.Read,
    		produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> listScenes(
    		@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("experimentId") final String experimentId,
    		@RequestParam(required=true, value="sceneFile") final String sceneFile
    		) throws NrgServiceException {
		if (!_preferences.getWbSceneDownloadEnabled(projectId)) {
			return new ResponseEntity<List<String>>(
					Arrays.asList(new String[] { "ERROR:  Scene file download is not enabled for this project" }),
					HttpStatus.BAD_REQUEST);
		}
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		final ClientInfo clientInfo = new ClientInfo(ClientCommand.LIST_SCENES, projectId, 
				mrSession.getLabel(), sceneFile, null);
    	try {
			return new ResponseEntity<List<String>>(WbSceneHelperClient.executeCommand(clientInfo),HttpStatus.OK);
		} catch (WbSceneDownloadClientException | ScriptExecException e) {
			log.error(ExceptionUtils.getFullStackTrace(e));
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

	@ApiOperation(value = "Downloads WB Scene File and Scenes", notes = "Downloads all scenes from WB scene file.", 
						responseContainer = "List", response = String.class)
    @ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(
    		value = {"/workbenchSceneDownload/project/{projectId}/experiment/{experimentId}/downloadSceneFile"}, 
    		restrictTo=AccessLevel.Read,
    		produces = {DOWNLOAD_MEDIA_TYPE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> downloadSceneFile(
    		@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("experimentId") final String experimentId,
    		@RequestParam(required=true, value="sceneFile") final String sceneFile
    		) throws NrgServiceException {
		if (!_preferences.getWbSceneDownloadEnabled(projectId)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    	try {
    		final ClientInfo clientInfo = new ClientInfo(ClientCommand.ZIP_SCENE_FILE, projectId, 
    				mrSession.getLabel(), URLDecoder.decode(sceneFile, "UTF-8"), null);
			List<String> returnList = WbSceneHelperClient.executeCommand(clientInfo);
			if (returnList == null || returnList.size() != 1) {
				log.error("ERROR:  WbSceneHelperClient.executeCommand return null or empty list (ClientCommand=" + ClientCommand.LIST_SCENE_FILES + ")");
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			final File returnFile = new File(returnList.get(0));
			if (!returnFile.exists()) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		    Path path = Paths.get(returnFile.getAbsolutePath());
	    	ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
		    try {
		    	return ResponseEntity.ok()
		    			.header(HttpHeaders.CONTENT_TYPE, DOWNLOAD_MEDIA_TYPE)
		    			.header(HttpHeaders.CONTENT_DISPOSITION, String.format(ATTACHMENT_DISPOSITION, returnFile.getName()) + ";")
		    			.contentLength(returnFile.length())
		    			.body(resource);
		    } finally {
		    	if (resource != null) {
		    		resource.getInputStream().close();
		    		resource = null;
		    	}
		    	cleanupTempDirectory(returnFile);
		    }
		} catch (WbSceneDownloadClientException | ScriptExecException | IOException e) {
			log.error(ExceptionUtils.getFullStackTrace(e));
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

	private void cleanupTempDirectory(File returnFile) {
		if (returnFile == null || ! returnFile.exists()) {
			return;
		}
		File tempDir = returnFile.getParentFile();
		if (tempDir.isDirectory() && tempDir.getName().endsWith(TMPDIR_SUFFIX)) {
			try {
				FileUtils.deleteDirectory(tempDir);
			} catch (IOException e) {
				// Do nothing
			}
		}
	}

	@ApiOperation(value = "Downloads Single Scene From WB Scene File", notes = "Downloads a single scene from a WB scene file.", 
						responseContainer = "List", response = String.class)
    @ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(
    		value = {"/workbenchSceneDownload/project/{projectId}/experiment/{experimentId}/downloadScene"}, 
    		restrictTo=AccessLevel.Read,
    		produces = {DOWNLOAD_MEDIA_TYPE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> downloadScene(
    		@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("experimentId") final String experimentId,
    		@RequestParam(required=true, value="sceneFile") final String sceneFile,
    		@RequestParam(required=true, value="sceneNumber") final Integer sceneNumber
    		) throws NrgServiceException {
		if (!_preferences.getWbSceneDownloadEnabled(projectId)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    	try {
    		final ClientInfo clientInfo = new ClientInfo(ClientCommand.ZIP_SCENE, projectId, 
    				mrSession.getLabel(), URLDecoder.decode(sceneFile, "UTF-8"), sceneNumber);
			final List<String> returnList = WbSceneHelperClient.executeCommand(clientInfo);
			if (returnList == null || returnList.size() != 1) {
				log.error("ERROR:  WbSceneHelperClient.executeCommand return null or empty list (ClientCommand=" + ClientCommand.LIST_SCENE_FILES + ")");
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			final File returnFile = new File(returnList.get(0));
			if (!returnFile.exists()) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		    Path path = Paths.get(returnFile.getAbsolutePath());
	    	ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
		    try {
		    	return ResponseEntity.ok()
		    			.header(HttpHeaders.CONTENT_TYPE, DOWNLOAD_MEDIA_TYPE)
		    			.header(HttpHeaders.CONTENT_DISPOSITION, String.format(ATTACHMENT_DISPOSITION, returnFile.getName()) + ";")
		    			.contentLength(returnFile.length())
		    			.body(resource);
		    } finally {
		    	if (resource != null) {
		    		resource.getInputStream().close();
		    		resource = null;
		    	}
		    	cleanupTempDirectory(returnFile);
		    }
		} catch (WbSceneDownloadClientException | ScriptExecException | IOException e) {
			log.error(ExceptionUtils.getFullStackTrace(e));
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
}
