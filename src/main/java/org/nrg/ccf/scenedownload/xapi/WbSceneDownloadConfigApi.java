package org.nrg.ccf.scenedownload.xapi;

import org.nrg.ccf.scenedownload.components.ProjectPreferences;
import org.nrg.ccf.scenedownload.pojos.WbSceneDownloadPreferences;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@XapiRestController
@Api(description = "Workbench Scene Download Configuration API")
public class WbSceneDownloadConfigApi extends AbstractXapiRestController {

	private final ProjectPreferences _preferences;
	
	@Autowired
	protected WbSceneDownloadConfigApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
								final ProjectPreferences preferences) {
		super(userManagementService, roleHolder);
		_preferences = preferences;
	}

	@ApiOperation(value = "Gets WB Scene Download project settings", notes = "Returns WB Scene Download project-level settings.", response = WbSceneDownloadPreferences.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/workbenchSceneDownloadConfig/{projectId}/settings"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<WbSceneDownloadPreferences> getWbSceneDownloadProjectSettings(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		if (_preferences == null) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
    	return new ResponseEntity<>(_preferences.getWbSceneDownloadPreferences(projectId),HttpStatus.OK);
    }
	
    @ApiOperation(value = "Sets WB Scene Download project settings", notes = "Sets WB Scene Download project-level settings.", response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/workbenchSceneDownloadConfig/{projectId}/settings"}, restrictTo=AccessLevel.Owner, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setWbSceneDownloadProjectSettings(@PathVariable("projectId") @ProjectId final String projectId, 
    			@RequestBody final WbSceneDownloadPreferences prefs) throws NrgServiceException {
		if (_preferences == null) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
    	_preferences.setWbSceneDownloadEnabled(projectId, 
    			(prefs.getWbSceneDownloadEnabled()!=null) ? prefs.getWbSceneDownloadEnabled() : false);
    	return new ResponseEntity<>(HttpStatus.OK);
    }
	
    @ApiOperation(value = "Is Workbench Scene Download Enabled?", notes = "Check Workbench Scene Download access for project.", response = Void.class)
    @XapiRequestMapping(value = {"/workbenchSceneDownloadConfig/{projectId}/isEnabled"}, restrictTo=AccessLevel.Authenticated, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Void> hasWbSceneDownloadAccess(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
    	if (_preferences != null && _preferences.getWbSceneDownloadEnabled(projectId) != null && _preferences.getWbSceneDownloadEnabled(projectId)) {
    		return new ResponseEntity<>(HttpStatus.OK);
    	}
   		return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
	
}
