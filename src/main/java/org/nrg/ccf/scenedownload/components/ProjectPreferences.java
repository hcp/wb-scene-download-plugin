package org.nrg.ccf.scenedownload.components;

import java.util.List;

import org.nrg.ccf.common.utilities.abst.AbstractProjectPreferenceBean;
import org.nrg.ccf.common.utilities.components.PreferenceUtils;
import org.nrg.ccf.scenedownload.pojos.WbSceneDownloadPreferences;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.exceptions.UnknownToolId;
import org.nrg.prefs.services.NrgPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@NrgPreferenceBean(toolId = "wbSceneDownloadProjectPreferences", toolName = "Workbench Scene File Download Plugin Project Preferences",
			description = "Manages workbench scene file download plugin project preferences", strict = true)
public class ProjectPreferences extends AbstractProjectPreferenceBean {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5200478383265998143L;
	public static final String WB_SCENE_DOWNLOAD_ENABLED = "wbSceneDownloadEnabled";
	private static final String _wbSceneDownloadEnabledProjectsQuery = "SELECT DISTINCT entity_id FROM xhbm_preference WHERE name = '" + WB_SCENE_DOWNLOAD_ENABLED + "' and value='true'";
	private final JdbcTemplate _jdbcTemplate;
	
	@Autowired
	public ProjectPreferences(NrgPreferenceService preferenceService, JdbcTemplate jdbcTemplate, PreferenceUtils preferenceUtils) {
		super(preferenceService, preferenceUtils);
		_jdbcTemplate = jdbcTemplate;
	}
	
	// Make PreferenceBeanHelper happy.  It wants the annotation to be on a getter with no arguments.
	@NrgPreference
	@SuppressFBWarnings
	public Boolean getWbSceneDownloadEnabled() {
		return null;
	}
	
	public Boolean getWbSceneDownloadEnabled(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, WB_SCENE_DOWNLOAD_ENABLED);
	}
	
	public void setWbSceneDownloadEnabled(final String entityId, final boolean enabled) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		if (entityId==null || entityId.length()<1) {
			// Don't set value.  This is a project preference.
			log.debug("WB_SCENE_DOWNLOAD Enabled preference - empty/null entity value passed");
			return;
		}
		try {
			log.debug("WB_SCENE_DOWNLOAD Enabled preference - Setting value for entity=" + entityId);
			this.setBooleanValue(SCOPE, entityId, enabled, WB_SCENE_DOWNLOAD_ENABLED);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			log.error("Exception setting workbench scene download enabled preference", e);
		}
	}
	
	public List<String> getWbSceneDownloadEnabledProjects() {
		return _jdbcTemplate.queryForList(_wbSceneDownloadEnabledProjectsQuery, String.class);
	}
	
	public WbSceneDownloadPreferences getWbSceneDownloadPreferences(String projectId) {
		final WbSceneDownloadPreferences prefs = new WbSceneDownloadPreferences();
		prefs.setWbSceneDownloadEnabled(this.getWbSceneDownloadEnabled(projectId));
		return prefs;
	}
	
}
