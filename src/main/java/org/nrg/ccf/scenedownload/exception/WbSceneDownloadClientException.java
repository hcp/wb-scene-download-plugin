package org.nrg.ccf.scenedownload.exception;

public class WbSceneDownloadClientException extends Exception {
	
	private static final long serialVersionUID = -8633439954053632737L;

	public WbSceneDownloadClientException() {
		super();
	}

	public WbSceneDownloadClientException(String message) {
		super(message);
	}

	public WbSceneDownloadClientException(Throwable cause) {
		super(cause);
	}

	public WbSceneDownloadClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public WbSceneDownloadClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
