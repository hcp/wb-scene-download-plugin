package org.nrg.ccf.scenedownload.pojos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class ClientInfo {
	
	@NonNull
	private ClientCommand command;
	private String project;
	private String experiment;
	private String sceneFile;
	private Integer scene;

}
