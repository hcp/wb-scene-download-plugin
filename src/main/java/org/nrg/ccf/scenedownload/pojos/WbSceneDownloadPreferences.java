package org.nrg.ccf.scenedownload.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WbSceneDownloadPreferences {
	
	private Boolean wbSceneDownloadEnabled;
	
}
