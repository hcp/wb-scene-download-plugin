package org.nrg.ccf.scenedownload.pojos;

public enum ClientCommand {
	
	LIST_SCENE_FILES, ZIP_SCENE_FILE, LIST_SCENES, ZIP_SCENE

}
