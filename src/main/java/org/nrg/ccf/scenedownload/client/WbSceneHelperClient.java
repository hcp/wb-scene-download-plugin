package org.nrg.ccf.scenedownload.client;

import java.util.List;

import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.common.utilities.utils.ScriptExecUtils;
import org.nrg.ccf.commons.utilities.exception.ScriptExecException;
import org.nrg.ccf.scenedownload.exception.WbSceneDownloadClientException;
import org.nrg.ccf.scenedownload.pojos.ClientInfo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WbSceneHelperClient {
	
	final static String SCENE_FILE_HELPER_COMMAND = System.getProperty("user.home") + "/bin/sceneFileHelper";

	public static List<String> executeCommand(ClientInfo info) throws WbSceneDownloadClientException, ScriptExecException {
		final StringBuilder sb = new StringBuilder(SCENE_FILE_HELPER_COMMAND);
		sb.append(" ").append(info.getCommand()).append(" ").append(info.getProject())
			.append(" ").append(info.getExperiment());
		if (info.getSceneFile() != null) {
			sb.append(" ").append(info.getSceneFile());
		}
		if (info.getScene() != null) {
			sb.append(" ").append(info.getScene());
		}
		log.info("Executing sceneFileHelperCommand:  " + sb.toString());
		return executeCommand(sb.toString()).getListResult();
	}
	
	protected static ScriptResult executeCommand(String cmd) throws WbSceneDownloadClientException, ScriptExecException {
		ScriptResult result = ScriptExecUtils.execRuntimeCommand(cmd);
		if (!result.isSuccess()) {
			throw new WbSceneDownloadClientException("ERROR:  Status request execution was not successful");
		}
		return result;
	}

}
