package org.nrg.ccf.scenedownload.conf;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "wbSceneDownloadPlugin",
			name = "Workbench Scene Download Plugin"
		)
@ComponentScan({ 
		"org.nrg.ccf.scenedownload.conf",
		"org.nrg.ccf.scenedownload.components",
		"org.nrg.ccf.scenedownload.utils",
		"org.nrg.ccf.scenedownload.xapi"
	})
@Slf4j
public class WbSceneDownloadPlugin {
	
	public WbSceneDownloadPlugin() {
		log.info("Configuring the Workbench Scene Download Plugin.");
	}
	
}
